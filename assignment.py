import boto3
import json
import os
import paramiko
import time

with open("config.json", "r") as jsonfile:
    config = json.load(jsonfile)

key_pair_name = config["key_pair_name"]
security_group = config["security_group_name"]
availabilityzones = config["availabilityzone"]
data = config["user_data"]
dns_mapping = {}
private_dns = []

rs_query = """mongo -u mongoadmin -p mongoadmin `hostname`/admin --ssl --sslCAFile /etc/ssl/mongo_ssl/CA.pem 
--sslPEMKeyFile /etc/ssl/mongo_ssl/mclient.pem --eval 'rs.initiate({_id: "TestRS-0",members: [{ _id: 0, 
host : "{}:27017" }, { _id: 1, host : "{}:27017" }, { _id: 2, host : "{}:27017", "arbiterOnly": true}]})' """


def create_key_pair(key_pair_name):
    global private_key
    print(key_pair_name)
    ec2_client = boto3.client("ec2")
    for key_pair in ec2_client.describe_key_pairs(DryRun=False)['KeyPairs']:
        if key_pair['KeyName'] == str(key_pair_name):
            print("key_pair is already present")
            return key_pair['KeyName']
    key_pair = ec2_client.create_key_pair(KeyName=str(key_pair_name))
    print(key_pair)
    private_key = key_pair["KeyMaterial"]
    print(private_key, file=open('{}.pem'.format(key_pair_name), 'w'))
    print(private_key)

    # #with open('mongo-key-pair.pem', 'w') as f:
    # print(private_key, file=open('mongo-key-pair.pem', 'a'))
    return key_pair['KeyName']


def create_check_security_group(security_group):
    ec2 = boto3.client('ec2')
    for rds_security_group in ec2.describe_security_groups()['SecurityGroups']:
        if rds_security_group['GroupName'] == str(security_group):
            print("Security Group is already present :" + rds_security_group['GroupId'])
            return rds_security_group['GroupId']
    # Security Group was not found, create it
    rds_security_group_name = ec2.create_security_group(
        GroupName=str(security_group),
        Description='mongodb-security-group')
    print(rds_security_group_name['GroupId'])
    ec2.authorize_security_group_ingress(
        GroupId=rds_security_group_name['GroupId'],
        IpPermissions=[
            {'IpProtocol': 'tcp',
             'FromPort': 22,
             'ToPort': 22,
             'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
            {'IpProtocol': 'icmp',
             'FromPort': -1,
             'ToPort': -1,
             'IpRanges': [{'CidrIp': '0.0.0.0/0'}]},
            {'IpProtocol': 'tcp',
             'FromPort': 27017,
             'ToPort': 27017,
             'UserIdGroupPairs': [{'GroupId': rds_security_group_name['GroupId']}],
             },
            {'IpProtocol': 'tcp',
             'FromPort': 3000,
             'ToPort': 3000,
             'IpRanges': [{'CidrIp': '0.0.0.0/0'}]
             },
            {'IpProtocol': 'tcp',
             'FromPort': 9090,
             'ToPort': 9090,
             'IpRanges': [{'CidrIp': '0.0.0.0/0'}]
             },
            {'IpProtocol': 'tcp',
             'FromPort': 9216,
             'ToPort': 9216,
             'IpRanges': [{'CidrIp': '0.0.0.0/0'}]
             },
        ])
    print(rds_security_group_name)
    print(rds_security_group_name['GroupId'])


def create_instance(security_group_id, availabilityzone, key_name):
    ec2_client = boto3.client("ec2")
    response = ec2_client.run_instances(
        BlockDeviceMappings=[
            {
                'DeviceName': '/dev/sda2',
                'Ebs': {
                    'DeleteOnTermination': True,
                    'VolumeSize': 10,
                    'VolumeType': 'gp2'
                },
            },
        ],
        Placement={
            'AvailabilityZone': str(availabilityzone)
        },
        KeyName=str(key_name),
        ImageId='ami-0b28dfc7adc325ef4',
        UserData=data,
        InstanceType='t2.micro',
        MaxCount=1,
        MinCount=1,
        Monitoring={
            'Enabled': False
        },
        SecurityGroupIds=[
            str(security_group_id),
        ],
    )
    print(response["Instances"][0]["InstanceId"])
    return response["Instances"][0]["InstanceId"]


def get_public_dns(instance_id):
    """having the instance_id, gives you the public DNS"""
    ec2_client = boto3.client("ec2")
    instance = ec2_client.describe_instances().get('Reservations')
    for reservation in instance:
        for instance in reservation['Instances']:
            if instance['InstanceId'] == instance_id:
                publicdns = instance['PublicDnsName']
                privatedns = instance['PrivateDnsName']
    return publicdns, privatedns


def ssh_connect(public_ip, cmd):
    # Join the paths using directory name and file name, to avoid OS conflicts
    key_path = os.path.join('/home/abhishek/PycharmProjects/pythonProject/Assignment', 'mongo-db-key.pem')

    key = paramiko.RSAKey.from_private_key_file(key_path)
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    # Connect/ssh to an instance
    while True:
        try:
            client.connect(hostname=public_ip, username="ec2-user", pkey=key)

            # Execute a command after connecting/ssh to an instance
            stdin, stdout, stderr = client.exec_command(cmd)
            print(stdout.read())

            # close the client connection once the job is done
            client.close()
            break

        except Exception as e:
            print(e)


def ssh_copy(public_ip):
    # Join the paths using directory name and file name, to avoid OS conflicts
    key_path = os.path.join('/home/abhishek/PycharmProjects/pythonProject/Assignment', 'mongo-db-key.pem')

    key = paramiko.RSAKey.from_private_key_file(key_path)
    os.popen("scp -i {} monitoring.sh ec2-user@{}:/home/ec2-user".format(key, public_ip))


def main():
    print("creating/Checking key pair \n-----------------------------------")
    key_name = create_key_pair(key_pair_name)
    print("creating Security Group \n-----------------------------------")
    security_group_id = create_check_security_group(security_group)
    print("creating Ec2 Instance \n-----------------------------------")
    for availabilityzone in availabilityzones:
        instance = create_instance(security_group_id, availabilityzone, key_name)
        time.sleep(30)
        publicdns, privatedns = get_public_dns(instance)
        print("Publicdns: " + publicdns, "Privatedns: " + privatedns)
        dns_mapping.update({availabilityzone: publicdns})
        private_dns.append(privatedns)
    time.sleep(200)
    for key in dns_mapping:
        if key == "us-west-2c":
            ssh_connect(dns_mapping[key], "bash /home/ec2-user/scripts/mongodb_replica.sh")
        else:
            ssh_connect(dns_mapping[key], "bash /home/ec2-user/scripts/mongodb_replica.sh")
