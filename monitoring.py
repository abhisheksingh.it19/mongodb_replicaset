#!/bin/bash
#set debug mode
#installing Grafana
sudo yum update -y
ins=$(cat <<- END
[grafana]
name=grafana
baseurl=https://packages.grafana.com/oss/rpm
repo_gpgcheck=1
enabled=1
gpgcheck=1
gpgkey=https://packages.grafana.com/gpg.key
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt
END
)
echo "$ins" | sudo tee /etc/yum.repos.d/grafana.repo

yes Y | sudo yum install -y grafana
sudo systemctl daemon-reload
sudo systemctl start grafana-server
sudo systemctl enable grafana-server.service

#installing Prometheus
sudo useradd --no-create-home prometheus
sudo mkdir /etc/prometheus
sudo mkdir /var/lib/prometheus

wget https://github.com/prometheus/prometheus/releases/download/v2.30.0/prometheus-2.30.0.linux-amd64.tar.gz
tar xvfz prometheus-2.30.0.linux-amd64.tar.gz

sudo cp prometheus-2.30.0.linux-amd64/prometheus /usr/local/bin
sudo cp prometheus-2.30.0.linux-amd64/promtool /usr/local/bin/
sudo cp -r prometheus-2.30.0.linux-amd64/consoles /etc/prometheus
sudo cp -r prometheus-2.30.0.linux-amd64/console_libraries /etc/prometheus

sudo cp prometheus-2.30.0.linux-amd64/promtool /usr/local/bin/
rm -rf prometheus-2.30.0.linux-amd64.tar.gz prometheus-2.30.0.linux-amd64

prom=$(cat <<- END
[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
Group=prometheus
Type=simple
ExecStart=/usr/local/bin/prometheus \
    --config.file /etc/prometheus/prometheus.yml \
    --storage.tsdb.path /var/lib/prometheus/ \
    --web.console.templates=/etc/prometheus/consoles \
    --web.console.libraries=/etc/prometheus/console_libraries

[Install]
WantedBy=multi-user.target
END
)
echo "$prom" | sudo tee /etc/systemd/system/prometheus.service

sudo chown prometheus:prometheus /etc/prometheus
sudo chown prometheus:prometheus /usr/local/bin/prometheus
sudo chown prometheus:prometheus /usr/local/bin/promtool
sudo chown -R prometheus:prometheus /etc/prometheus/consoles
sudo chown -R prometheus:prometheus /etc/prometheus/console_libraries
sudo chown -R prometheus:prometheus /var/lib/prometheus

sudo systemctl daemon-reload
sudo systemctl enable prometheus

cd /home/ec2-user/
mkdir mongodb-exporter
cd mongodb-exporter
wget https://github.com/percona/mongodb_exporter/releases/download/v0.20.7/mongodb_exporter-0.20.7.linux-amd64.tar.gz
tar xvzf mongodb_exporter-0.7.1.linux-amd64.tar.gz
cd mongodb_exporter-0.20.7.linux-amd64/
sudo mv mongodb_exporter /usr/local/bin/
mongo `hostname` --ssl --sslCAFile /etc/ssl/mongo_ssl/CA.pem --sslPEMKeyFile /etc/ssl/mongo_ssl/mclient.pem << EOF
use admin
db.createUser(
  {
    user: "mongodb_exporter",
    pwd: "password",
    roles: [
        { role: "clusterMonitor", db: "admin" },
        { role: "read", db: "local" }
    ]
  }
)
EOF

mongo_expor=$(cat <<- END
[Unit]
Description=MongoDB Exporter
User=prometheus

[Service]
Type=simple
Restart=always
ExecStart=/usr/local/bin/mongodb_exporter

[Install]
WantedBy=multi-user.target
END)
sudo systemctl daemon-reload
sudo systemctl start mongodb_exporter.service
