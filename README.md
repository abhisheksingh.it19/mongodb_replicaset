# mongodb_replicaset Summary

A replica set in MongoDB is a group of mongod processes that maintain the same data set. Replica sets provide redundancy and high availability, and are the basis for all production deployments.

Key features of replication:
- Scalability: As the data volume increases the complexity of accessing data and working with data also increases. With replication in place, multiple data copies are available, allowing users to not only increase their data reserves but also recover any previous version in case of any errors or failures.
- Performance: When data is available across multiple machines and servers, it not only makes accessing data easier but also makes recovering from unexpected and sudden failures much easier. replication ensures data availability and security at all times.
- Availability: With replication in place, there’s no need to worry about data failures. In situations where your primary source of data fails, you can easily access the same up-to-date data from a secondary reserve. This highly promotes data availability.

## Prerequists
Setup client through aws configure
for more details follow below steps
```
aws configure
<access_key>
<secret_key>
<region>

```

## Getting started

**Run the script "assignment.py" it will create below resources on AWS**
- Key-pair if not exist
- security group & security group ingress rules
- ec2 instances 
- mongodb installation, adding few parameters on kernal level & setup of certs along with keys

```
python assignment.py
```

## once script is triggered wait for few minutes till mongodb came up fully after that run below commands.

**To check mongodb came up or not use below command**
```
sudo service mongod status
```

```
mongo `hostname` --ssl --sslCAFile /etc/ssl/mongo_ssl/CA.pem --sslPEMKeyFile /etc/ssl/mongo_ssl/mclient.pem
use admin
db.createUser({ user: "mongoadmin" , pwd: "mongoadmin", roles: [{"role": "root", "db": "admin"}]})
db.auth("mongoadmin", "mongoadmin")
exit
```

## open /etc/mongod.conf & remove comments of security & replica set part then restart mongod service & initiate replica set

```
sudo sed -i 's/#security/security/g' /etc/mongod.conf
sudo sed -i 's/#  authorization/  authorization/g' /etc/mongod.conf
sudo sed -i 's/#  keyFile/  keyFile/g' /etc/mongod.conf
sudo sed -i 's/#replication/replication/g' /etc/mongod.conf
sudo sed -i 's/#  replSetName/  replSetName/g' /etc/mongod.conf
sudo service mongod restart

#to eanble replicaset
mongo -u mongoadmin -p mongoadmin `hostname`/admin --ssl --sslCAFile /etc/ssl/mongo_ssl/CA.pem --sslPEMKeyFile /etc/ssl/mongo_ssl/mclient.pem --eval 'rs.initiate({_id: "TestRS-0",members: [{ _id: 0, host : "ip-172-31-25-140.us-west-2.compute.internal:27017" }, { _id: 1, host : "ip-172-31-43-210.us-west-2.compute.internal:27017" }, { _id: 2, host : "ip-172-31-4-228.us-west-2.compute.internal:27017", "arbiterOnly": true}]})'
```


